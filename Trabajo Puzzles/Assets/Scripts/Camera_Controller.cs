using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    Vector2 mouseMirar;
    Vector2 suavidadV;

    public float sens = 5.0f;
    public float suavizado = 2.0f;

    public Texture2D crosshair;
    public Texture2D intCrosshair;
    private Texture2D currentCross;

    public float rayDistance;
    public LayerMask rayLayer;

    GameObject player;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        player = this.transform.parent.gameObject;
        currentCross = crosshair;
    }

    private void Update()
    {
        var md = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sens * suavizado, sens * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, player.transform.up);

        CheckForInteractiveObject();
        

        if (Input.GetMouseButtonDown(0))
        {
            Interact();
        }
    }

    private void Interact()
    {
        GameObject obj = SearchObject();
        if (obj != null)
        {
            Drawer_Controller drawer = obj.GetComponent<Drawer_Controller>();
            if (drawer != null) drawer.OpenClose();
        } 
    }
    private GameObject SearchObject()
    {
        Camera cam = Camera.main;
        if (cam != null)
        {
            // Realizar el raycast desde la posición de la cámara hacia adelante
            Ray ray = new Ray(cam.transform.position, cam.transform.forward);
            RaycastHit hit;

            // Comprobar si el raycast ha colisionado con algún objeto
            if (Physics.Raycast(ray, out hit, rayDistance, rayLayer))
            {
                // Imprimir el nombre del objeto colisionado
                Debug.Log("Objeto detectado: " + hit.collider.name);
                return hit.transform.gameObject;

                // Realizar cualquier acción adicional aquí, como interactuar con el objeto
            }

            // (Opcional) Dibujar el raycast en la escena para depuración
            Debug.DrawRay(cam.transform.position, cam.transform.forward * rayDistance, Color.red);
        }

        return null;    
    }

    private void CheckForInteractiveObject()
    {
        Camera cam = Camera.main;
        if (cam != null)
        {
            Ray ray = new Ray(cam.transform.position, cam.transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, rayDistance, rayLayer))
            {
                if (hit.transform.CompareTag("Cajon"))
                {
                    currentCross = intCrosshair;
                    return;
                }
            }
        }

        currentCross = crosshair;
    }
    private void OnGUI()
    {
        Rect rect = new Rect(Screen.width / 2, Screen.height / 2, crosshair.width, crosshair.height);
        GUI.DrawTexture(rect, currentCross);
    }
}
