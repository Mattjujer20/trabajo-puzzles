using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pick_Up_Items : MonoBehaviour
{
    public GameObject CrowbarOnPlayer;
    private GameObject touchPlayer;
    public int itemID;

    void Start()
    {
        CrowbarOnPlayer.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && touchPlayer)
        {
            this.gameObject.SetActive(false);

            CrowbarOnPlayer.SetActive(true);

            touchPlayer.GetComponent<Control_Inventory>().ActivarObj(itemID);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            touchPlayer = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            touchPlayer = null;
        }
    }
}
