using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight_Reveal : MonoBehaviour
{
    public Material revealedMaterial;
    public Light flashlight;
    public Transform playerCamera;

    void Update()
    {
        Vector3 lightPos = flashlight.transform.position + flashlight.transform.forward * 10.0f;
        Vector3 screenPos = playerCamera.GetComponent<Camera>().WorldToViewportPoint(lightPos);
        revealedMaterial.SetVector("_LightPos", new Vector4(screenPos.x, screenPos.y, 0, 0));
    }
}
