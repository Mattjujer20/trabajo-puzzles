using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionTriggerUI : MonoBehaviour
{
    // Referencia al objeto UI (imagen o canvas) que se mostrará al jugador
    [SerializeField] private GameObject interactionUI;

    // Nombre del tag del jugador para asegurarte de que solo se active con el jugador
     private string playerTag = "Player";

    private void Start()
    {
        // Asegúrate de que el UI esté desactivado al inicio
        if (interactionUI != null)
        {
            interactionUI.SetActive(false);
        }
        else
        {
            Debug.LogWarning("Interaction UI no está asignado en " + gameObject.name);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entra tiene el tag de jugador
        if (other.CompareTag(playerTag) && interactionUI != null)
        {
            interactionUI.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Desactiva el UI cuando el jugador sale del collider
        if (other.CompareTag(playerTag) && interactionUI != null)
        {
            interactionUI.SetActive(false);
        }
    }
}
