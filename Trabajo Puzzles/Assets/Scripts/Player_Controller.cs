using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    public float desplazamiento = 10.0f;
    public Vector3 checkpointPosition;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        checkpointPosition = transform.position;
    }

    private void Update()
    {
        float movAtras = Input.GetAxis("Vertical") * desplazamiento;
        float movCostado = Input.GetAxis("Horizontal") * desplazamiento;

        movAtras *= Time.deltaTime;
        movCostado *= Time.deltaTime;

        transform.Translate(movCostado, 0, movAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (transform.position.y < -4)
        {
            TeleportToCheckpoint();
        }
    }

    private void TeleportToCheckpoint()
    {
        transform.position = checkpointPosition;
    }
}
