using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reveal_Obj : MonoBehaviour
{
    public Material revealedMaterial;
    public Material normalMaterial;
    private Renderer objectRenderer;

    void Start()
    {
        objectRenderer = GetComponent<Renderer>();
        objectRenderer.material = normalMaterial;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("FlashlightBeam"))
        {
            objectRenderer.material = revealedMaterial;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("FlashlightBeam"))
        {
            objectRenderer.material = normalMaterial;
        }
    }
}
