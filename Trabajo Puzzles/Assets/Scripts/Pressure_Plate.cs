using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pressure_Plate : MonoBehaviour
{
    public Transform door;
    public Transform openDoor;
    public Transform closeDoor;

    public bool unLocked = false;
    public float speed = 1.0f;
    Vector3 targetPosition;
    float time;

    private void Start()
    {
        targetPosition = closeDoor.position;
    }

    private void Update()
    {
        if (unLocked && door.position != targetPosition)
        {
            door.transform.position = Vector3.Lerp(door.transform.position, targetPosition, time);
            time += Time.deltaTime * speed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if ( other.tag == "PressurePlate")
        {
            targetPosition = openDoor.position;
            time = 0;
        }
    }

    
}
