using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public Key_Door openedDoor;
    public GameObject doorMessageCanvas; // Asigna el Canvas del mensaje de la puerta en el Inspector
    public GameObject keyDisplayCanvas;

    private bool isPlayerNear = false; // Variable para saber si el jugador está cerca

    private void Update()
    {
        if (isPlayerNear && Input.GetKeyDown(KeyCode.E)) // Verificar si el jugador presiona "E"
        {
            Debug.Log("agarrando llave");
            openedDoor.candado = true;
            Destroy(gameObject); // Destruir la llave después de recogerla
            AkSoundEngine.PostEvent("Play_Llave", gameObject);

            if (keyDisplayCanvas != null)
            {
                keyDisplayCanvas.SetActive(true);
            }

            // Activar el canvas de mensaje de la puerta si está asignado
            if (doorMessageCanvas != null)
            {
                Debug.Log("mostrando");
                doorMessageCanvas.SetActive(true);
                StopAllCoroutines();
                StartCoroutine(HideCanvasAfterDelay());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerNear = true; // El jugador está cerca
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerNear = false; // El jugador se ha alejado
        }
    }

    private IEnumerator HideCanvasAfterDelay()
    {
        

        Debug.Log("Esconder canvas");
       
       
            Debug.Log("se escoondio");
            doorMessageCanvas.SetActive(true);
            yield return new WaitForSeconds(5f); // Esperar 2 segundos
            doorMessageCanvas.SetActive(false); // Desactivar el canvas después de 2 segundos
        
    }
}