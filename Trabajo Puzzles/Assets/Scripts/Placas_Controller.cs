using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placas_Controller : MonoBehaviour
{
    public string[] combination = { "1", "3", "4", "2" };
    private int indiceActual = 0;

    public Material correctMaterial;
    public Material incorrectMaterial;

    public GameObject moveCube;
    public Vector3 newPosCube;

    private List<ObjectWithMaterial> objectChanges = new List<ObjectWithMaterial>();

    private class ObjectWithMaterial
    {
        public GameObject objeto;
        public Material OriginalMat;
    }

    private void OnCollisionEnter(Collision collision)
    {
        string tiquetColision = collision.collider.tag;

        if (CorrectColor(tiquetColision))
        {
            ChangeColor(collision.collider.gameObject, correctMaterial);
            indiceActual++;

            if (indiceActual == combination.Length)
            {
                MoveCube();
                RestartCombination();
            }
        }
        else
        {
            foreach (ObjectWithMaterial changeObject in objectChanges)
            {
                ResetColor(changeObject);
            }

            objectChanges.Clear();
            RestartCombination();
        }
    }

    private bool CorrectColor(string tiquet)
    {
        return indiceActual < combination.Length && tiquet == combination[indiceActual];
    }

    private void RestartCombination()
    {
        indiceActual = 0;
    }

    private void ChangeColor(GameObject obj, Material newMat)
    {
        Renderer render = obj.GetComponent<Renderer>();

        if (render != null)
        {
            Material originalMat = render.material;
            render.material = newMat;
            objectChanges.Add(new ObjectWithMaterial { objeto = obj, OriginalMat = originalMat });
        }
    }

    private void ResetColor(ObjectWithMaterial changeObject)
    {
        Renderer render = changeObject.objeto.GetComponent<Renderer>();

        if (render != null)
        {
            render.material = changeObject.OriginalMat;
        }
    }

    private void MoveCube()
    {
        StartCoroutine(MoveDestiny(moveCube.transform, newPosCube));
    }

    private IEnumerator MoveDestiny(Transform objectTransform, Vector3 destiny)
    {
        float iniciateTime = Time.time;
        Vector3 iniciatePos = objectTransform.position;
        float duration = 4.0f;

        while (Time.time - iniciateTime < duration)
        {
            float passTime = Time.time - iniciateTime;
            float completeFraction = passTime / duration;

            objectTransform.position = Vector3.Lerp(iniciatePos, destiny, completeFraction);
            yield return null;
        }

        objectTransform.position = destiny;
    }
}