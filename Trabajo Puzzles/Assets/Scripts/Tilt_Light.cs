using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Tilt_Light : MonoBehaviour
{
    public bool lightTilt = false;
    public float timeDelay;

    IEnumerator LightTilting()
    {
        lightTilt = true;
        this.gameObject.GetComponent<Light>().enabled = false;
        timeDelay = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(timeDelay);
        this.gameObject.GetComponent<Light>().enabled = true;
        timeDelay = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(timeDelay);
        lightTilt = false;

    }

    private void Update()
    {
        if(lightTilt == false)
        {
            StartCoroutine(LightTilting());
        }
    }
}
