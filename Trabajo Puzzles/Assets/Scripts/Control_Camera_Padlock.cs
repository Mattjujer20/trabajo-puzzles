using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Camera_Padlock : MonoBehaviour
{
    public Camera mainCamera;
    public Camera camaraCerradura;
    public GameObject player;
    public MonoBehaviour playerMovementScript; // Arrastra el script de movimiento del jugador aquí en el Inspector

    private bool isLookingThroughKeyhole = false;

    void Start()
    {
        // Asegúrate de que la cámara principal esté activa y la cámara de la cerradura esté inactiva al inicio
        mainCamera.gameObject.SetActive(true);
        camaraCerradura.gameObject.SetActive(false);
    }

    void Update()
    {
        if (isLookingThroughKeyhole && Input.GetKeyDown(KeyCode.Escape))
        {
            // Si estamos mirando por la cerradura y presionamos Escape, volvemos a la cámara principal
            ToggleCameras(false);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.E))
        {
            // Si el jugador está en el trigger y presiona E, cambiamos de cámara
            ToggleCameras(!isLookingThroughKeyhole);
        }
    }

    private void ToggleCameras(bool lookThroughKeyhole)
    {
        isLookingThroughKeyhole = lookThroughKeyhole;

        mainCamera.gameObject.SetActive(!lookThroughKeyhole);
        camaraCerradura.gameObject.SetActive(lookThroughKeyhole);

        // Activar/desactivar el script de movimiento del jugador
        playerMovementScript.enabled = !lookThroughKeyhole;

        if (lookThroughKeyhole)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
