using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key_Display : MonoBehaviour
{
    public Image keyImage; // La imagen de la llave en el canvas

    private void Start()
    {
        // Asegúrate de que la imagen de la llave esté desactivada al inicio
        keyImage.enabled = false;
    }

    public void ShowKey()
    {
        // Activa la imagen de la llave
        keyImage.enabled = true;
    }

    public void HideKey()
    {
        // Desactiva la imagen de la llave si es necesario
        keyImage.enabled = false;
    }
}
