using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Digit_Door : MonoBehaviour
{
    public GameObject player;
    private Animator anim;
    private bool isDoor;
    [SerializeField] private TextMeshProUGUI codeText;
    string codeTextValue = "";
    public string safeCode;
    public GameObject codePanel;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    
    void Update()
    {
        if (!player)
        {
            return;
        }
        codeText.text = codeTextValue;

        if(codeTextValue == safeCode)
        {
            anim.SetTrigger("OpenDoor");
            codePanel.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if(codeTextValue.Length > 4)
        {
            codeTextValue = "";
        }
           
        if(Input.GetKeyUp(KeyCode.E) && isDoor == true)
        {
            codePanel.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if(isDoor == false)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isDoor = true;
            player = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            isDoor = false;
            codePanel.SetActive(false);
            player = null;
        }
    }

    public void AddDigit(string digit)
    {
        codeTextValue += digit;
    }
}
