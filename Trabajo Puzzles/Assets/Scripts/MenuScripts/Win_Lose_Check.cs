using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win_Lose_Check : MonoBehaviour
{
    [Header("UI Settings")]
    [Tooltip("La imagen que se mostrará al ganar")]
    public GameObject winImage; // La imagen que aparecerá en pantalla.

    [Tooltip("Texto de victoria (opcional)")]
    public Text winText; // Texto dentro de la imagen para mostrar un mensaje.

    [Tooltip("Mensaje que deseas mostrar al ganar")]
    public string victoryMessage = "¡Ganaste!"; // Mensaje por defecto.

    private void Start()
    {
        // Asegúrate de que la imagen de victoria esté desactivada al inicio
        if (winImage != null)
        {
            winImage.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entra al Trigger tiene la etiqueta "Player"
        if (other.CompareTag("Player"))
        {
            // Activa la imagen de victoria
            if (winImage != null)
            {
                winImage.SetActive(true);
            }

            // Actualiza el texto si es necesario
            if (winText != null)
            {
                winText.text = victoryMessage;
            }

            // Pausa el juego
            Time.timeScale = 0f;
        }
    }

    // Método para reanudar el juego (puedes llamarlo desde un botón)
    public void ResumeGame()
    {
        // Desactiva la imagen de victoria y reanuda el tiempo
        if (winImage != null)
        {
            winImage.SetActive(false);
        }
        Time.timeScale = 1f;
    }
}
