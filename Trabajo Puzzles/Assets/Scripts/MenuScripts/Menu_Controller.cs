using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_Controller : MonoBehaviour
{

    public void StartGame(string LevelName)
    {
        SceneManager.LoadScene(LevelName);
    }
    public void Exit ()
    {
        Application.Quit ();
        Debug.Log("Saliendo");
    }
}
