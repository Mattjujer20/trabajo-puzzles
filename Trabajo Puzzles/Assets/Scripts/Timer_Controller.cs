using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement; // Para reiniciar la escena

public class Timer_Controller : MonoBehaviour
{
    [Header("Configuración del Temporizador")]
    [Tooltip("Tiempo inicial en minutos")]
    public float timeInMinutes = 25f; // Tiempo inicial en minutos, configurable desde el inspector

    private float time; // Tiempo en segundos
    public TextMeshProUGUI contador; // Texto que muestra el tiempo

    private void Start()
    {
        // Convertir minutos a segundos
        time = timeInMinutes * 60f;
        UpdateTimerDisplay();
    }

    private void Update()
    {
        if (time > 0)
        {
            // Reducir el tiempo en función del deltaTime
            time -= Time.deltaTime;

            // Evitar números negativos
            time = Mathf.Max(time, 0);

            // Actualizar la visualización del temporizador
            UpdateTimerDisplay();
        }
        else
        {
            // Reiniciar la escena si el tiempo llega a 0
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    // Método para actualizar el texto del temporizador en formato mm:ss
    private void UpdateTimerDisplay()
    {
        int minutes = Mathf.FloorToInt(time / 60f);
        int seconds = Mathf.FloorToInt(time % 60f);
        contador.text = $"{minutes:00}:{seconds:00}";
    }
}