using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key_Door : MonoBehaviour
{
    public Transform door;
    public Transform openDoor;
    public Transform closeDoor;

    public bool candado;

    public float speedDoor = 1f;
    Vector3 targetPosition;
    float time;

    private bool isPlayerNear = false; // Variable para saber si el jugador está cerca
    public GameObject doorMessageCanvas; // Arrastra el Canvas del mensaje aquí en el inspector
    public GameObject keyDisplayCanvas;

    private void Start()
    {
        targetPosition = closeDoor.position;

        if (doorMessageCanvas != null)
        {
            doorMessageCanvas.SetActive(false); // Asegúrate de que el Canvas esté oculto al inicio
        }
        
    }

    private void Update()
    {
        if (candado && door.position != targetPosition)
        {
            door.transform.position = Vector3.Lerp(door.transform.position, targetPosition, time);
            time += Time.deltaTime * speedDoor;
        }

        if (isPlayerNear && Input.GetKeyDown(KeyCode.E))
        {
            if (!candado) // Si el candado no está abierto
            {
                Debug.Log("La puerta está cerrada. Mostrando mensaje.");
                StartCoroutine(ShowDoorMessage());
            }
            else
            {
                targetPosition = openDoor.position;
                time = 0;
                AkSoundEngine.PostEvent("Play_Puerta", gameObject);

                // Desactivar el canvas de la llave si está asignado
                if (keyDisplayCanvas != null)
                {
                    keyDisplayCanvas.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerNear = true; // El jugador está cerca
            Debug.Log("Jugador cerca de la puerta");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerNear = false; // El jugador se ha alejado
            Debug.Log("Jugador se alejó de la puerta");
        }
    }

    private IEnumerator ShowDoorMessage()
    {
        if (doorMessageCanvas != null)
        {
            doorMessageCanvas.SetActive(true);
            yield return new WaitForSeconds(2); // Mostrar el mensaje por 2 segundos
            doorMessageCanvas.SetActive(false);
        }
        else
        {
            Debug.LogError("doorMessageCanvas no está asignado en el Inspector");
        }
    }
}
