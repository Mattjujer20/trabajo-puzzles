using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_UpDown : MonoBehaviour
{
    bool down = false;
    int speed = 3;

    private void Update()
    {
        if(transform.position.y >= 7)
        {
            down = true;    
        }
        if(transform.position.y <= 0)
        {
            down = false;
        }
        if (down)
        {
            moveDown();
        }
        else
        {
            moveUp();
        }
    }

    private void moveUp()
    {
        transform.position += transform.up * speed * Time.deltaTime;
    }

    private void moveDown()
    {
        transform.position -= transform.up * speed * Time.deltaTime;
    }
}
