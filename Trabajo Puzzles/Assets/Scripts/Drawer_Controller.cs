using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer_Controller : MonoBehaviour
{
    public float speed;
    public Vector3 openPosition;
    public float whenOpen;

    public float diffOpenedDrawer;

    bool open = false;
    bool opening = false; 
    bool closing = false;
    Vector3 closePosition;


    void Start()
    {
        closePosition = transform.localPosition;
        openPosition = new Vector3(transform.localPosition.x - whenOpen, transform.localPosition.y, transform.localPosition.z);

    }

    
    void Update()
    {
        //diffOpenedDrawer = closePosition.x - transform.localPosition.x;

        if (opening)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, closePosition, Time.deltaTime * speed);
            if (Vector3.Distance(transform.localPosition, closePosition) < 0.00001f)
            {
                open = true;
                opening = false;
            }
        }

        if (closing)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, openPosition, Time.deltaTime * speed);
            if(Vector3.Distance(transform.localPosition, openPosition) < 0.00001f)
            {
                open = false;
                closing = false;
            }
        }

        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //   OpenClose();
        //}
    }

    public void OpenClose()
    {
        if (!open)
        {
            opening = true;
        }
        else
        {
            closing = true;
        }
    }
}
