using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Inventory : MonoBehaviour
{
    public GameObject crowbar;
    public GameObject flashlight;
    public bool isCrowbar, isFlashlight;

    private void Start()
    {
        crowbar.SetActive(false);
        flashlight.SetActive(false);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && isCrowbar)
        {
            if(crowbar.activeSelf)
            {
                crowbar.SetActive(false);
                return;
            }
            DesactivarObj();
            crowbar.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && isFlashlight)
        {
            if(flashlight.activeSelf)
            {
                flashlight.SetActive(false);
                return;
            }
            DesactivarObj();
            flashlight.SetActive(true);
        }
    }

    private void DesactivarObj()
    {
        crowbar.SetActive(false);
        flashlight.SetActive(false);
    }

    public void ActivarObj(int id) 
    { 
        if(id == 1)
        {
            isCrowbar = true;
        }

        if(id == 2)
        {
            isFlashlight = true;
        }
    }
}
