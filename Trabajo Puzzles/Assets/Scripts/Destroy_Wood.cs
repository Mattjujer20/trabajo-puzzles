using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_Wood : MonoBehaviour
{
    public GameObject objectToDisable; // Referencia al objeto que quieres desactivar en el inspector
    public GameObject crowbar; // Referencia al objeto Crowbar (1) en el inspector

    private bool isPlayerNear = false; // Variable para saber si el jugador está cerca

    private void Update()
    {
        if (isPlayerNear && Input.GetMouseButtonDown(0)) // Verificar si el jugador presiona el botón izquierdo del mouse
        {
            if (crowbar != null && crowbar.activeSelf) // Verificar si la crowbar está activa
            {
                if (objectToDisable != null)
                {
                    objectToDisable.SetActive(false); // Desactivar el objeto
                }
                else
                {
                    Debug.LogError("No se ha asignado el objeto a desactivar en el inspector.");
                }
            }
            else
            {
                Debug.Log("Necesitas tener la crowbar equipada para romper esto.");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerNear = true; // El jugador está cerca
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerNear = false; // El jugador se ha alejado
        }
    }


}
