using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Nota : MonoBehaviour
{
    public GameObject notaVisual;
    private bool activa;
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && activa == true)
        {
            notaVisual.SetActive(true);
            AkSoundEngine.PostEvent("Play_Hoja", gameObject);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && activa == true)
        {
            notaVisual.SetActive(false);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            activa = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            activa = false;
            notaVisual.SetActive(false);
        }
    }
}
